package com.example.alumnedam.voyamorir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class Formulari extends AppCompatActivity {

    private EditText nom;
    private EditText dia;
    private EditText mes;
    private EditText ano;
    private RadioButton hombre;
    private RadioButton mujer;
    private RadioButton si;
    private RadioButton no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulario);

        final Button Calcular = (Button) findViewById(R.id.Calcular);
        Calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nom = (EditText) findViewById(R.id.m_etNombre);
                dia = (EditText) findViewById(R.id.m_etDia);
                mes = (EditText) findViewById(R.id.m_etMes);
                ano = (EditText) findViewById(R.id.m_etAno);
                hombre = (RadioButton) findViewById(R.id.m_rbHombre);
                mujer = (RadioButton) findViewById(R.id.m_rbMujer);
                si = (RadioButton) findViewById(R.id.m_rdSi);
                no = (RadioButton) findViewById(R.id.m_rdNo);


                if(nom.getText().toString().equals("") || dia.getText().toString().equals("") || mes.getText().toString().equals("") || ano.getText().toString().equals("") ) {

                } else {
                    String nombre = nom.getText().toString();
                    int dias = Integer.parseInt(dia.getText().toString());
                    int mess = Integer.parseInt(mes.getText().toString());
                    int anos = Integer.parseInt(ano.getText().toString());
                    Intent res=new Intent(Formulari.this,Resultado.class);
                    res.putExtra("nombre",nombre);
                    res.putExtra("dia",dias);
                    res.putExtra("mes",mess);
                    res.putExtra("ano",anos);
                    res.putExtra("si",si.isChecked());
                    res.putExtra("hombre",hombre.isChecked());
                    startActivity(res);
                }
            }
        });
    }

    public void openResultado(Info info){
        Intent i = new Intent(this, Resultado.class);
        startActivity(i);
    }

}
