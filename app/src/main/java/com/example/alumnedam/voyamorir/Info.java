package com.example.alumnedam.voyamorir;

/**
 * Created by alumneDAM on 05/02/2018.
 */

public class Info {

    int dia;
    int mes;
    int ano;
    String nombre;
    boolean casado;
    boolean hombre;

    public Info(int dia, int mes, int ano, String nombre, boolean casado, boolean hombre) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        this.nombre = nombre;
        this.casado = casado;
        this.hombre = hombre;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isCasado() {
        return casado;
    }

    public void setCasado(boolean casado) {
        this.casado = casado;
    }

    public boolean isHombre() {
        return hombre;
    }

    public void setHombre(boolean hombre) {
        this.hombre = hombre;
    }
}
