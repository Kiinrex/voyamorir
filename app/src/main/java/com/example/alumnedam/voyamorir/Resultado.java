package com.example.alumnedam.voyamorir;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Resultado extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        TextView frase = (TextView) findViewById(R.id.Frase);

        String nombre = getIntent().getExtras().getString("nombre");
        int dia = getIntent().getExtras().getInt("dia");
        int mes = getIntent().getExtras().getInt("mes");
        int ano = getIntent().getExtras().getInt("ano");
        boolean casado = getIntent().getExtras().getBoolean("si");
        boolean hombre = getIntent().getExtras().getBoolean("hombre");
        ano = ano + 60;
        String[] muertes = getResources().getStringArray(R.array.Muertes);

        if(mes<=2) {
            if(casado)
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[0]+ " " +getResources().getString(R.string.Casado));
            else
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[0]+ " " +getResources().getString(R.string.NoCasado));
        } else if(mes>2 && mes<=4){
            if(casado)
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[1]+ " " +getResources().getString(R.string.Casado));
            else
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[1]+ " " +getResources().getString(R.string.NoCasado));
        } else if(mes>4 && mes<=6){
            if(casado)
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[2]+ " " +getResources().getString(R.string.Casado));
            else
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[2]+ " " +getResources().getString(R.string.NoCasado));
        } else if(mes>6 && mes<=8){
            if(casado)
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[3]+ " " +getResources().getString(R.string.Casado));
            else
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[3]+ " " +getResources().getString(R.string.NoCasado));
        } else if(mes>8 && mes<=10){
            if(casado)
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[4]+ " " +getResources().getString(R.string.Casado));
            else
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[4]+ " " +getResources().getString(R.string.NoCasado));
        } else if(mes>10){
            if(casado)
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[5]+ " " +getResources().getString(R.string.Casado));
            else
                frase.setText(nombre + getResources().getString(R.string.morir)+ " "  + dia+ "/" + mes + "/" + ano+ "\n"+ muertes[5]+ " " +getResources().getString(R.string.NoCasado));
        }
    }
}
